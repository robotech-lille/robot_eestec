#include <WiFi.h>
#include <HTTPClient.h>
#include <Wire.h>

#define L_FORWARD 27
#define L_BACK 26
#define L_STEP 33

#define R_FORWARD 18
#define R_BACK 5
#define R_STEP 17

const int freq = 5000;//fréquence PWM -> 5 kHz
const int resolution = 8;//résolution PWM -> 8 bits

const char* ssid = "robotEestec_02";//ssid de la manette
const char* password = "eestec_02_robot";//mot de passe de la manette

//ip de la manette avec les chemins pour récupérer la vitesse et la direction
const char* serverNameSpeed = "http://192.168.4.1/speed";
const char* serverNameDirection = "http://192.168.4.1/direction";

String speedStr;
String directionStr;

int speedValue;
int directionValue;

int l;
int r;

unsigned long previousMillis = 0;
const long interval = 100;//délai pour les appels manette 100 ms -> 10 Hz

void setup() {
  Serial.begin(115200);//sérial de debug

  //configuration des pins
  pinMode(L_FORWARD, OUTPUT);
  pinMode(L_BACK, OUTPUT);
  pinMode(L_STEP, OUTPUT);
  pinMode(R_FORWARD, OUTPUT);
  pinMode(R_BACK, OUTPUT);
  pinMode(R_STEP, OUTPUT);

  //création des channels pour le PWM
  ledcSetup(0, freq, resolution);
  ledcSetup(1, freq, resolution);

  //on attache un pin à une channel PWM
  ledcAttachPin(R_STEP, 0);
  ledcAttachPin(L_STEP, 1);

  //connexion au wifi
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  //wifi connecté
  Serial.println(WiFi.localIP());
}

void loop() {
  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis >= interval) { //test si la manette est toujours connectée
    if(WiFi.status()== WL_CONNECTED ){
      speedStr = httpGETRequest(serverNameSpeed);//récupération de la vitesse auprès de la manette
      directionStr = httpGETRequest(serverNameDirection);//récupération de la direction auprès de la mantte

      previousMillis = currentMillis;
    }
    else {//arret des moteurs en cas de déconnexion de la manette
      Serial.println("WiFi Disconnected");
      speedStr = "2048";//joystick en position centrale
      directionStr = "2048";//koystick en position centrale
    }

    speedValue = map(speedStr.toInt(),0,4096,-255 ,255 );
    directionValue = map(directionStr.toInt(),0,4096,-128 ,128 );
    
    l = constrain(speedValue - directionValue, -255, 255);
    r = constrain(speedValue + directionValue, -255, 255);
    write_motor(l, 0, L_FORWARD, L_BACK);
    delay(5);
    write_motor(r, 1, R_FORWARD, R_BACK);

    Serial.print("left: ");
    Serial.print(l);
    Serial.print( " - right: ");
    Serial.println(r);
  }
}

void write_motor(int val, int channel, int back_pin, int forward_pin){

  if (val > 0){//gestion de la direction
    digitalWrite(forward_pin, HIGH) ;
    digitalWrite(back_pin, LOW);
  } else {
    digitalWrite(forward_pin, LOW) ;
    digitalWrite(back_pin, HIGH);
  }

  ledcWrite(channel, abs(val));//écriture PWM
}


String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;

  http.begin(client, serverName);
  int httpResponseCode = http.GET();

  String payload = "--";

  if (httpResponseCode>0) {
    int httpCode = httpResponseCode;
    if(httpCode!=200){
      Serial.print("HTTP Response code: ");
      Serial.println(httpCode);
    }
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  http.end();

  return payload;
}
