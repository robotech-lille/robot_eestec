#include "WiFi.h"
#include "ESPAsyncWebServer.h"
#include "tft.h"

#define JL_X 26
#define JL_Y 32
#define JR_X 36
#define JR_Y 13

const char* ssid = "robotEestec_01";//ssid du wifi généré
const char* password = "eestec_01_robot";//mot de passe du wifi généré

IPAddress staticIP(192, 168, 4, 1);//fixage de l'ip

int LvalX = 0, LvalY = 0, RvalX = 0, RvalY = 0;
int lx = 0, ly = 0, rx = 0, ry =0;

//tft init
TFT_eSPI tft = TFT_eSPI(135, 240);

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

uint16_t rgb(uint8_t red, uint8_t green, uint8_t blue){
  return (((red & 0b11111000)<<8) + ((green & 0b11111100)<<3)+(blue>>3));
}

void initColor(){
  tft.fillScreen(rgb(0,0,0));

  tft.setTextColor(rgb(255,255,255));
  tft.setTextDatum(MC_DATUM);

  tft.setRotation(2);
  tft.setTextSize(1);
  tft.setCursor(0, 0);
}

void drawBackground(){
  tft.drawString(ssid,  67, 0);
  tft.drawRect(0,170,60,60,rgb(255,255,255));
  tft.drawRect(70,170,60,60,rgb(255,255,255));
}

String getSpeed(){
  return String(LvalY);
}

String getDirection(){
  return String(RvalX);
}

void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  Serial.println();

  //setting display
  tft.init();
  if (TFT_BL > 0) {
    pinMode(TFT_BL, OUTPUT);
    digitalWrite(TFT_BL, TFT_BACKLIGHT_ON);
  }
  tft.setSwapBytes(true);
  initColor();
  drawBackground();

  // setting pins
  pinMode(JL_X, INPUT);
  pinMode(JL_Y, INPUT);
  pinMode(JR_X, INPUT);
  pinMode(JR_Y, INPUT);

  // Setting the ESP as an access point
  Serial.print("Setting AP (Access Point)…");
  WiFi.softAP(ssid, password,3,1,1);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  server.on("/speed", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getSpeed().c_str());
  });
  server.on("/direction", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getDirection().c_str());
  });

  bool status;
  // Start server
  server.begin();
}

void loop(){
  tft.drawCircle(lx,ly,2,rgb(0,0,0));
  tft.drawCircle(rx,ry,2,rgb(0,0,0));

  LvalX = 2048;
  LvalY = analogRead(JL_Y);
  Serial.print("L : ");
  Serial.print(LvalY);

  RvalX = analogRead(JR_X);
  Serial.print(" R : ");
  Serial.println(RvalX);
  RvalY = 2048;

  lx = map(LvalX,0,4096,26,-27)+30;
  ly = map(LvalY,0,4096,-27,26)+200;
  rx = map(RvalX,0,4096,26,-27)+100;
  ry = map(RvalY,0,4096,-27,26)+200;

  tft.drawCircle(lx,ly,2,rgb(255,0,0));
  tft.drawCircle(rx,ry,2,rgb(255,0,0));
  delay(8);
}
